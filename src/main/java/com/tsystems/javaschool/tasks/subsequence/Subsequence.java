package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean result=false;
        // TODO: Implement the logic here

        if (x==null || y==null){
            throw new IllegalArgumentException();
        }
        List z = new ArrayList<>();
        int k =0;
        for (int i = 0; i < x.size() ; i++) {
            for (int j = k; j < y.size(); j++) {
                if (x.get(i)==y.get(j)){
                    z.add(y.get(j));
                    k=j;
                    break;
                }
            }
        }
        if (z.size()==x.size()){
            return result=true;
        }else{
            return result=false;
        }
    }
}
