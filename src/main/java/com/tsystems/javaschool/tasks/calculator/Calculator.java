package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement==null){
            return null;
        }
        String result;
        int res = (int) Result(Postfix(statement));
        double res2 = Result(Postfix(statement)) - res;
        if (Result(Postfix(statement)) == 0) {
            result = null;
        } else if(res2 == 0){
            result = Integer.toString(res);
        } else{
            result = Double.toString(Result(Postfix(statement)));
        }

        return result;
    }

    StringBuilder seq = new StringBuilder();
    Stack<Character> stack = new Stack<Character>();

    public String Postfix(String input) {
        if (input.contains("++") || input.contains("--") || input.contains("**") || input.contains("//")){
            return "0";
        }
        if (input.isEmpty()) {
            return "0";
        }
        if (input =="0") {
            return "0";
        }
        if (input == "null") {
            return "0";
        }
        for (int i = 0; i < input.length(); i++) {
            if (Prior(input.charAt(i)) == 0) {
                seq.append(input.charAt(i));
            }

            if (Prior(input.charAt(i)) == -1) {
                while (!stack.isEmpty() && Prior(stack.peek()) != 1) {
                    if (!stack.isEmpty()) {
                        seq.append(stack.pop());
                    }
                }
                if (!stack.isEmpty()) {
                    stack.pop();
                }
                continue;
            }

            if (Prior(input.charAt(i)) > 0) {
                seq.append(" ");
                if (stack.isEmpty()) {
                    stack.push(input.charAt(i));
                } else if (Prior(stack.peek()) < Prior(input.charAt(i))) {
                    stack.push(input.charAt(i));
                } else if (Prior(stack.peek()) == Prior(input.charAt(i))) {
                    seq.append(stack.pop());
                    stack.push(input.charAt(i));
                } else if (Prior(stack.peek()) > Prior(input.charAt(i))) {
                    if (input.charAt(i) == '(') {
                        stack.push(input.charAt(i));
                    } else {
                        seq.append(stack.pop());
                        stack.push(input.charAt(i));

                    }


                }
            }
        }

        while (!stack.isEmpty()) {
            seq.append(stack.pop());
        }
        return seq.toString();
    }
    public double Result(String inp) {
        int chek = 1;
        if (chek_error(inp) == 0) {
            return 0;
        }
        Stack<Double> res_stack = new Stack<Double>();

        for (int i = 0; i < inp.length(); i++) {
            if (inp.charAt(i) == ' ') {
                continue;
            }
            if (Prior(inp.charAt(i)) == 0 || inp.charAt(i) == '.') {
                String num = "";
                while (Prior(inp.charAt(i)) == 0 && inp.charAt(i) != ' ') {
                    num += inp.charAt(i++);
                }
                try {
                    Double d = Double.parseDouble(num);
                    res_stack.push(d);
                } catch (NumberFormatException e) {
                    chek = 0;
                    break;
                }
            }
            if (Prior(inp.charAt(i)) > 0) {
                double n1;
                double n2;
                n2 = res_stack.pop();
                n1 = res_stack.pop();
                switch (inp.charAt(i)) {
                    case '+':
                        res_stack.push(n2 + n1);
                        break;
                    case '-':
                        res_stack.push(n1 - n2);
                        break;
                    case '*':
                        res_stack.push(n2 * n1);
                        break;
                    case '/':
                        if (n2 == 0) {
                            return 0;
                        } else {
                            res_stack.push(n1 / n2);
                        }
                        break;
                }
            }
        }
        if (chek == 0) {
            return chek;
        } else {
            double value = res_stack.pop();
            value = new BigDecimal(value).setScale(4, RoundingMode.HALF_EVEN).doubleValue();
            return value;
        }

    }

    private int chek_error(String inp) {
        int chek = 1;
        if (inp.equals("0")) {
            chek = 0;
        }
        if (Prior(inp.charAt(0)) != 0) {
            chek = 0;
        }
        for (int i = 0; i < inp.length(); i++) {
            if (inp.charAt(i) == '(' || inp.charAt(i) == ')') {
                chek = 0;
            }
        }
        return chek;
    }


    static int Prior(Character a) {
        if (a == '(') {
            return 1;
        }
        if (a == ')') {
            return -1;
        }
        if (a == '+' || a == '-') {
            return 2;
        }
        if (a == '*' || a == '/') {
            return 3;
        } else {
            return 0;
        }
    }
}
