package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Stack;

public class PyramidBuilder {

    public int cntElem;
    public int nLine;
    public int nCol;
    public int chek = 1;


    public int[][] buildPyramid(List<Integer> inputNumbers) {


        if (inputNumbers.size() > Integer.MAX_VALUE - 5) {
            throw new CannotBuildPyramidException();
        }

        for (Object i : inputNumbers
        ) {
            if (i == null) {
                throw new CannotBuildPyramidException();
            }
        }

        int[] inpArr = new int[inputNumbers.size()];
        for (int i = 0; i < inpArr.length; i++)
            inpArr[i] = inputNumbers.get(i);
        cntElem = inpArr.length;

        chek_conditions();

        if (chek==1){
            sort(inpArr);
        }

        int[][] pyram = new int[nLine][nCol];

        if (chek == 1) {
            Stack<Integer> stack = new Stack<>();
            for (int i = inpArr.length-1; i >-1 ; i--) {
                stack.push(inpArr[i]);
            }
            for (int i = 0; i < nLine; i++) {
                for (int j = (nCol-1)/2-i; j <(nCol-1)/2+i+1 ; j+=2) {
                    pyram[i][j] = stack.pop();
                }
            }
        }

        return pyram;
    }

    public void chek_conditions() {
        double discr = Math.sqrt(1 + 8 * cntElem);
        if (discr % 1 == 0) {
            nLine = (int) (-1 + discr) / 2;
            nCol = nLine * 2 - 1;
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    public int[]  sort(int[] input) {
        boolean b = true;
        while (b) {
            b = false;
            for (int i = 1; i < input.length; i++) {
                if (input[i - 1] > input[i]) {
                    b = true;
                    int tmp = input[i];
                    input[i] = input[i - 1];
                    input[i - 1] = tmp;
                }
            }
        }
        return input;
    }
}
